package estaciones;

public final class LavadoLlantas extends Estacion {

	public static int tiempo;
	public static int precio;
	
	public static final void cargarEstacion(int tiempoConfig, int precioConfi) {
		LavadoLlantas.tiempo = tiempoConfig;
		LavadoLlantas.precio = precioConfi;
	}
	@Override
	public int getTiempo() {
		return LavadoLlantas.tiempo;
	}
	@Override
	public int getPrecio() {
		return LavadoLlantas.precio;
	}
	
}