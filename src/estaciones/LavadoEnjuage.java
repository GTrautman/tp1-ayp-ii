package estaciones;

public final class LavadoEnjuage extends Estacion {
	public static int tiempo;
	public static int precio;
	
	public static final void cargarEstacion(int tiempoConfig, int precioConfi) {
		tiempo = tiempoConfig;
		precio = precioConfi;
	}
	@Override
	public int getTiempo() {
		return tiempo;
	}
	@Override
	public int getPrecio() {
		return precio;
	}
	
}