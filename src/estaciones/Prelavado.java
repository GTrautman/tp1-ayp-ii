package estaciones;

public final class Prelavado extends Estacion {

	public static int tiempo;
	public static int precio;
	
	public static final void cargarEstacion(int tiempoConfig, int precioConfi) {
		Prelavado.tiempo = tiempoConfig;
		Prelavado.precio = precioConfi;
	}
	@Override
	public int getTiempo() {
		return Prelavado.tiempo;
	}
	@Override
	public int getPrecio() {
		return Prelavado.precio;
	}
}