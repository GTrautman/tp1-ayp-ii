package estaciones;

public final class Encerado extends Estacion {

	public static int tiempo;
	public static int precio;
	
	public static final void cargarEstacion(int tiempoConfig, int precioConfi) {
		Encerado.tiempo = tiempoConfig;
		Encerado.precio = precioConfi;
	}
	@Override
	public int getTiempo() {
		return Encerado.tiempo;
	}
	@Override
	public int getPrecio() {
		return Encerado.precio;
	}
}