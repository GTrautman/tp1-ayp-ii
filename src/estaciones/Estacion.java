package estaciones;

public abstract class Estacion {
	/*Atributos:
	 * 	tiempo: tiempo que tarda el cliente en la estacion
	 * 	precio: costo que cuesta entrar en la estacion
	 * 	parametros seteados mediante la clase Configuration Manager (leyendo archivo de configuraci�n)
	 */
	
	//Getters  & Setters
	public abstract int getTiempo();
	public abstract int getPrecio();
	
}