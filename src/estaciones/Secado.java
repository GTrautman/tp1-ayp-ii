package estaciones;


public final class Secado extends Estacion {

	public static int tiempo;
	public static int precio;
	
	public static final void cargarEstacion(int tiempoConfig, int precioConfi) {
		Secado.tiempo = tiempoConfig;
		Secado.precio = precioConfi;
	}
	@Override
	public int getTiempo() {
		return Secado.tiempo;
	}
	@Override
	public int getPrecio() {
		return Secado.precio;
	}
	
}