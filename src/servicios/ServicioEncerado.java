package servicios;

import estaciones.Encerado;

public class ServicioEncerado extends Servicio {
	//El servicio de Encerado no posee estaciones ya que es manual
	
	private static int cantidadDeVecesUsado = 0;
	private static int tiempoEsperaTotal = 0;
	
	public ServicioEncerado(){
		ServicioEncerado.listaEstaciones.clear();
		ServicioEncerado.listaEstaciones.add(new Encerado());
		ServicioEncerado.cantidadDeVecesUsado++;
	}
	
	public static int getCantidadDeVecesUsado() {
		return ServicioEncerado.cantidadDeVecesUsado;
	}

	public static int getTiempoEsperaTotal() {
		return ServicioEncerado.tiempoEsperaTotal;
	}
	

	public static void setTiempoEsperaTotal(int tiempoEsperaTotal) {
		ServicioEncerado.tiempoEsperaTotal += tiempoEsperaTotal;
	}
	
	public static void setCantidadDeVecesUsado(int cantidadDeVecesUsado) {
		ServicioEncerado.cantidadDeVecesUsado = cantidadDeVecesUsado;
	}

	public double getCostoTotalDelSerivicio(){
		return ServicioEncerado.getPrecioTotal()*ServicioEncerado.cantidadDeVecesUsado;
	}
}
