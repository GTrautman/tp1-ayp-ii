package servicios;

import estaciones.LavadoEnjuage;
import estaciones.LavadoLlantas;
import estaciones.Prelavado;
import estaciones.Secado;

public class ServicioCompleto extends Servicio {
	/*Constructor:
	 *	al inicializar el objeto se cargará automáticamente las estaciones que
	 *	pertenecen al "Servicio Completo" 	
	 */
	private static int cantidadDeVecesUsado = 0;
	private static int tiempoEsperaTotal = 0;
	
	public ServicioCompleto(){
		ServicioCompleto.listaEstaciones.clear();
		ServicioCompleto.listaEstaciones.add(new Prelavado());
		ServicioCompleto.listaEstaciones.add(new LavadoEnjuage());
		ServicioCompleto.listaEstaciones.add(new LavadoLlantas());
		ServicioCompleto.listaEstaciones.add(new Secado());
		
		ServicioCompleto.cantidadDeVecesUsado++;
	}
	
	public static int getCantidadDeVecesUsado() {
		return ServicioCompleto.cantidadDeVecesUsado;
	}
	
	public static int getTiempoEsperaTotal() {
		return ServicioCompleto.tiempoEsperaTotal;
	}
	
	public static void setTiempoEsperaTotal(int tiempoEsperaTotal) {
		ServicioCompleto.tiempoEsperaTotal += tiempoEsperaTotal;
	}
	
	public static void setCantidadDeVecesUsado(int cantidadDeVecesUsado) {
		ServicioCompleto.cantidadDeVecesUsado = cantidadDeVecesUsado;
	}
	
	public static double getCostoTotalDelSerivicio(){
		return ServicioCompleto.getPrecioTotal()*ServicioCompleto.cantidadDeVecesUsado;
	}

}
