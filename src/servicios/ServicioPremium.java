package servicios;

import estaciones.Brillo;
import estaciones.LavadoEnjuage;
import estaciones.LavadoLlantas;
import estaciones.Prelavado;
import estaciones.Secado;

public class ServicioPremium extends Servicio {
	/*Constructor:
	 *	al inicializar el objeto se cargará automáticamente las estaciones que
	 *	pertenecen al "Servicio Premium" 	
	 */
	private static int cantidadDeVecesUsado = 0;
	private static int tiempoEsperaTotal = 0;
	


	public ServicioPremium(){
		ServicioPremium.listaEstaciones.clear();
		ServicioPremium.listaEstaciones.add(new Prelavado());
		ServicioPremium.listaEstaciones.add(new LavadoEnjuage());
		ServicioPremium.listaEstaciones.add(new LavadoLlantas());
		ServicioPremium.listaEstaciones.add(new Secado());
		ServicioPremium.listaEstaciones.add(new Brillo());
		
		ServicioPremium.cantidadDeVecesUsado++;
	}
	
	public static int getCantidadDeVecesUsado() {
		return ServicioPremium.cantidadDeVecesUsado;
	}
	
	public static int getTiempoEsperaTotal() {
		return ServicioPremium.tiempoEsperaTotal;
	}
	
	public static void setTiempoEsperaTotal(int tiempoEsperaTotal) {
		ServicioPremium.tiempoEsperaTotal += tiempoEsperaTotal;
	}
	
	public static void setCantidadDeVecesUsado(int cantidadDeVecesUsado) {
		ServicioPremium.cantidadDeVecesUsado = cantidadDeVecesUsado;
	}
	
	public double getCostoTotalDelSerivicio(){
		return ServicioPremium.getPrecioTotal()*ServicioPremium.cantidadDeVecesUsado;
	}
}
