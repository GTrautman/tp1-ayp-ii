package servicios;

import estaciones.LavadoEnjuage;
import estaciones.LavadoLlantas;
import estaciones.Secado;

public class ServicioEconomico extends Servicio {
	/*Constructor:
	 *	al inicializar el objeto se cargará automáticamente las estaciones que
	 *	pertenecen al "Servicio Economico" 	
	 */
	private static int cantidadDeVecesUsado = 0;
	private static int tiempoEsperaTotal = 0;
	
	public ServicioEconomico(){
		ServicioEconomico.listaEstaciones.clear();
		ServicioEconomico.listaEstaciones.add(new LavadoEnjuage());
		ServicioEconomico.listaEstaciones.add(new LavadoLlantas());
		ServicioEconomico.listaEstaciones.add(new Secado());
		
		ServicioEconomico.cantidadDeVecesUsado++;
	}
	
	
	public static int getCantidadDeVecesUsado() {
		return ServicioEconomico.cantidadDeVecesUsado;
	}


	public static int getTiempoEsperaTotal() {
		return ServicioEconomico.tiempoEsperaTotal;
	}


	public static void setTiempoEsperaTotal(int tiempoEsperaTotal) {
		ServicioEconomico.tiempoEsperaTotal += tiempoEsperaTotal;
	}

	public static void setCantidadDeVecesUsado(int cantidadDeVecesUsado) {
		ServicioEconomico.cantidadDeVecesUsado = cantidadDeVecesUsado;
	}


	public double getCostoTotalDelSerivicio(){
		return ServicioEconomico.getPrecioTotal()*ServicioEconomico.cantidadDeVecesUsado;
	}

		
}
