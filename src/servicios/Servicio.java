package servicios;

import java.util.ArrayList;
import estaciones.Estacion;

public abstract class Servicio {
	//Listado de estaciones que contiene el Servicio
	protected static ArrayList<Estacion> listaEstaciones = new ArrayList<Estacion>();
	public Servicio(){	
	}
	//Recorre la lista de Estaciones y devuelve el tiempo total de ejecucion
	public int getTiempoTotal(){
		int tiempoTotalServicio = 0;
		for (int i=0; i < listaEstaciones.size(); i++){
			tiempoTotalServicio += listaEstaciones.get(i).getTiempo();
		}
		return tiempoTotalServicio;
	}
	
	public static int getPrecioTotal(){
		int precioTotal = 0;
		for (int i=0; i < listaEstaciones.size(); i++){
			precioTotal += listaEstaciones.get(i).getPrecio();
		}
		return precioTotal;
	}
}
