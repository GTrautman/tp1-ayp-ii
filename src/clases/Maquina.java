package clases;

import estaciones.Encerado;
import utils.Cola;
import utils.ColaException;

public class Maquina {
	
	public int tiempoSalida = 0;
	public int tiempoOcioso = 0;
	
	private int comienzoTiempoOcioso = 0;
	private int tiempoDuracionEncerado = Encerado.tiempo;
	
	private int clientesAtendidos = 0;
	private int tiempoDeAtencion = 0;
	
	private Cola cola = new Cola();
	private Cola colaEncerado = new Cola();
	
	public Ticket obtenerTop() {
		Ticket auto = null;
		if (!cola.colaVacia()) {			
			try {
				auto = cola.getPrimero();
			} catch (ColaException e) {
				e.printStackTrace();
			}
		}
		return auto;
	}
		
	public boolean maquinaVacia(){
		return this.cola.colaVacia();
	}

	public void evaluarLlegada(int tiempo) {

		int tiempoEspera = 0;
		
		try {
					
			if (tiempo < 720 && (cola.colaVacia() || tiempo < cola.getPrimero().getHoraSalidaEstimada())) {
				
				if (!cola.colaVacia()){
					tiempoEspera = this.tiempoSalida - tiempo; 
				}else{
					tiempoOcioso += tiempo - comienzoTiempoOcioso;
					this.tiempoSalida = tiempo;
				}
				
				Ticket nuevoAuto = new Ticket(tiempoEspera, tiempo);
				int tiempoAntencion = nuevoAuto.getTiempoAtencion();
				
				this.tiempoSalida += tiempoAntencion;

				nuevoAuto.setHoraSalidaEstimada(this.tiempoSalida);
				
				cola.encolar(nuevoAuto);
				this.clientesAtendidos++;
				this.tiempoDeAtencion += nuevoAuto.getTiempoAtencion();
				
				
			}else{
				while (!cola.colaVacia() && cola.getPrimero().getHoraSalidaEstimada() <= tiempo) {
					this.sacarAutoDeColaMaquina();
				}
				if (cola.colaVacia()) {
					comienzoTiempoOcioso = this.tiempoSalida;
				}
				if (tiempo < 720) {					
					this.evaluarLlegada(tiempo);
				}
			}
		} catch (ColaException e) {
			e.getMessage();
		}
		
		
	}


	private void sacarAutoDeColaMaquina() {
		try {
			Ticket autoSacado = cola.desencolar();
			if (autoSacado.getRequiereServicioOpcional()) {
				colaEncerado.encolar(autoSacado);
			}
		} catch (ColaException e) {
			e.printStackTrace();
		}
	}

	public void evaluarColaEncerado(int tiempo) {
		try {
			while (!colaEncerado.colaVacia() && (colaEncerado.getPrimero().getHoraSalidaEstimada() + this.tiempoDuracionEncerado) <= tiempo) {
				colaEncerado.desencolar();
			}
		} catch (ColaException e) {
			e.printStackTrace();
		}
		
	}

	public void vaciar() {
		while (!cola.colaVacia()) {
			this.sacarAutoDeColaMaquina();
		}
		while (!colaEncerado.colaVacia()) {
			try {
				colaEncerado.desencolar();
			} catch (ColaException e) {
				e.getMessage();
			}
		}		
	}

	public int getClientesAtendidos() {
		return clientesAtendidos;
	}

	public void setClientesAtendidos(int clientesAtendidos) {
		this.clientesAtendidos = clientesAtendidos;
	}

	public int getTiempoDeAtencion() {
		return tiempoDeAtencion;
	}

	public void setTiempoDeAtencion(int tiempoDeAtencion) {
		this.tiempoDeAtencion = tiempoDeAtencion;
	}
	
	
}
