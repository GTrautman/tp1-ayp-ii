	package clases;

import servicios.Servicio;
import servicios.ServicioCompleto;
import servicios.ServicioEconomico;
import servicios.ServicioEncerado;
import servicios.ServicioPremium;

public class Ticket {
	
	private int horaLlegada;
	private int horaSalidaEstimada;
	private Servicio servicio;
	private boolean requiereServicioOpcional;
	
	//Getters & Setters
	public int getHoraLlegada() {
		return horaLlegada;
	}
	public void setHoraLlegada(int horaLlegada) {
		this.horaLlegada = horaLlegada;
	}
	public int getHoraSalidaEstimada() {
		return horaSalidaEstimada;
	}
	public void setHoraSalidaEstimada(int horaSalidaEstimada) {
		this.horaSalidaEstimada = horaSalidaEstimada;
	}
	
	public boolean isRequiereServicioOpcional() {
		return requiereServicioOpcional;
	}
	public void setRequiereServicioOpcional(boolean requiereServicioOpcional) {
		this.requiereServicioOpcional = requiereServicioOpcional;
	}
	public boolean getRequiereServicioOpcional(){
		return this.requiereServicioOpcional;
	}
	public Servicio getServicio() {
		return servicio;
	}
	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}
	public int getTiempoAtencion(){
		return this.getServicio().getTiempoTotal();
	}
	/*	Construstor
	 * 		Crea un servicio determinado por un nro Random
	 */
	public Ticket(int tiempoDeEspera,int tiempoDeLlegada){
		this.setServicio(this.calcularServicioRandom(tiempoDeEspera));
		this.setRequiereServicioOpcional(this.calcularEncerado(tiempoDeEspera));
		this.horaLlegada = tiempoDeLlegada; 
	}
	// Devuelve un objeto del tipo Servicio dependiendo del n�mero Random que se haya elegido	
	private Servicio calcularServicioRandom(int tiempoDeEspera){
		int numeroRandom = (int) (Math.random()*100);
		if (numeroRandom >= 50){
			ServicioEconomico.setTiempoEsperaTotal(tiempoDeEspera);
			return new ServicioEconomico();
		}else if(numeroRandom >= 20 && numeroRandom < 50){
			ServicioCompleto.setTiempoEsperaTotal(tiempoDeEspera);
			return new ServicioCompleto();
		}else{
			ServicioPremium.setTiempoEsperaTotal(tiempoDeEspera);
			return new ServicioPremium();
		}
		
		
	}
	
	private boolean calcularEncerado(int tiempoDeEspera){
		boolean requiereServicioOpcional = ((int) (Math.random()*100) >= 50); 
		if(requiereServicioOpcional){
			ServicioEncerado.setTiempoEsperaTotal(tiempoDeEspera);
			new ServicioEncerado();
		}
		return requiereServicioOpcional;
		 
	}
	

}
