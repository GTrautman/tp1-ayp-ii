package clases;

import servicios.ServicioCompleto;
import servicios.ServicioEconomico;
import servicios.ServicioEncerado;
import servicios.ServicioPremium;

public class ResultadoSimulacion {
	/*ATRIBUTOS
	 *	Resultados que devolverá la Simulación luego de ser ejecutada
	 */
	private double promedioTiempoAtencion = 0;
	private double promedioEsperaGeneralEspera = 0;
	private double promedioEsperaServicioEconomico = 0;
	private double promedioEsperaServicioCompleto = 0;
	private double promedioEsperaServicioPremium = 0;
	private double promedioEsperaServicioEncerado = 0;

	//GETTERS & SETTERS

	public double getPromedioTiempoAtencion() {
		return promedioTiempoAtencion;
	}
	public void setPromedioTiempoAtencion(double promedioTiempoAtencion) {
		this.promedioTiempoAtencion = promedioTiempoAtencion;
	}

	public double getPromedioEsperaGeneralEspera() {
		return promedioEsperaGeneralEspera;
	}
	public void setPromedioEsperaGeneralEspera(double promedioEsperaGeneralEspera) {
		this.promedioEsperaGeneralEspera = promedioEsperaGeneralEspera;
	}
	public double getPromedioEsperaServicioEconomico() {
		return promedioEsperaServicioEconomico;
	}
	public void setPromedioEsperaServicioEconomico(
			double promedioEsperaServicioEconomico) {
		this.promedioEsperaServicioEconomico = promedioEsperaServicioEconomico;
	}
	public double getPromedioEsperaServicioCompleto() {
		return promedioEsperaServicioCompleto;
	}
	public void setPromedioEsperaServicioCompleto(
			double promedioEsperaServicioCompleto) {
		this.promedioEsperaServicioCompleto = promedioEsperaServicioCompleto;
	}
	public double getPromedioEsperaServicioPremium() {
		return promedioEsperaServicioPremium;
	}
	public void setPromedioEsperaServicioPremium(
			double promedioEsperaServicioPremium) {
		this.promedioEsperaServicioPremium = promedioEsperaServicioPremium;
	}
	public double getPromedioEsperaServicioEncerado() {
		return promedioEsperaServicioEncerado;
	}
	public void setPromedioEsperaServicioEncerado(
			double promedioEsperaServicioEncerado) {
		this.promedioEsperaServicioEncerado = promedioEsperaServicioEncerado;
	}
	
	public double getCostoDiario(){
		return (ServicioPremium.getCantidadDeVecesUsado() +
		 		  ServicioEconomico.getCantidadDeVecesUsado() +
		 		  ServicioCompleto.getCantidadDeVecesUsado() +
		 		  ServicioEncerado.getCantidadDeVecesUsado());
	}
	
	public double promedioTotalCostosServicio(){
		return this.getCostoDiario()/4;
	}
		
}
