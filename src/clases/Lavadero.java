package clases;

import servicios.ServicioCompleto;
import servicios.ServicioEconomico;
import servicios.ServicioEncerado;
import servicios.ServicioPremium;
import utils.ConfigurationManager;
import utils.ConfigurationManagerException;
import utils.PoissonSimulator;
import utils.PoissonSimulatorException;

//CLASE PRINCIPAL
public class Lavadero {

	private String ruta = "D:\\Facu\\lavado.txt";
	private int tiempoFinal = 720;
	
	/* Metodo principal
	 * 	ejecutara todo el programa, realizando colas de espera, calculos de salida y entrada, entre otros
	 * 	devuelve un objeto del tipo ResultadoSimulaci�n para luego ser leido
	 */
	public ResultadoSimulacion ejecutar(int personasPorDia){
		
		int tiempo = 0;
		int tiempoAEvaluar = 0;
		int cant = personasPorDia; //Este parametro se configura mediante el Configuration Manager (lectura del archivo txt)
		
		ConfigurationManager configuracion = new ConfigurationManager(this.ruta);
		try {
			configuracion.configurarEstaciones();
			configuracion.inicializarAtributosDeServicios();
		} catch (ConfigurationManagerException e) {
			e.getMessage();
		}
		
		
		Maquina maquina = new Maquina();

		do{
			int proximoAuto = this.proximoAuto(cant, tiempoFinal);
			tiempo += proximoAuto;
			
			maquina.evaluarLlegada(tiempo);
			maquina.evaluarColaEncerado(tiempo);			
			
			tiempoAEvaluar = maquina.tiempoSalida;
			
			if (tiempoAEvaluar >= tiempoFinal || personasPorDia == maquina.getClientesAtendidos()) {
				maquina.vaciar();
			}
			
			
			
		}while(tiempoAEvaluar < tiempoFinal && tiempo < tiempoFinal && personasPorDia > maquina.getClientesAtendidos());
		
		int clientesAtendidos = maquina.getClientesAtendidos();
		int sumatoriaTiempoAtencion = maquina.getTiempoDeAtencion();

		
		ResultadoSimulacion resultado = new ResultadoSimulacion();

		resultado.setPromedioTiempoAtencion(sumatoriaTiempoAtencion/clientesAtendidos);
		resultado.setPromedioEsperaServicioCompleto(promedioTiempos(ServicioCompleto.getTiempoEsperaTotal(), ServicioCompleto.getCantidadDeVecesUsado()));
		resultado.setPromedioEsperaServicioPremium(promedioTiempos(ServicioPremium.getTiempoEsperaTotal(), ServicioPremium.getCantidadDeVecesUsado()));
		resultado.setPromedioEsperaServicioEconomico(promedioTiempos(ServicioEconomico.getTiempoEsperaTotal(), ServicioEconomico.getCantidadDeVecesUsado()));
		resultado.setPromedioEsperaGeneralEspera((ServicioEconomico.getTiempoEsperaTotal() + ServicioEncerado.getTiempoEsperaTotal() +
						ServicioPremium.getTiempoEsperaTotal() + ServicioCompleto.getTiempoEsperaTotal())/clientesAtendidos );
		
		
		resultado.setPromedioEsperaServicioEncerado(promedioTiempos(ServicioEncerado.getTiempoEsperaTotal(), ServicioEncerado.getCantidadDeVecesUsado()));
		
		
		return resultado;
					
	}
	
	private int proximoAuto(int cant, int min){
		int proximo = 0;
		PoissonSimulator p;
		try {
			p = new PoissonSimulator(cant, min);
			do{
				proximo = (int) p.proximoArribo();
			}
			while(proximo == 0);
		} catch (PoissonSimulatorException e) {
			e.getMessage();
		}
		return proximo;
	}
	
	public void iniciarSimulacion(){
		ConfigurationManager configuracion = new ConfigurationManager(this.ruta);
		int[] dias = null;
		try {
			dias = configuracion.configurarDias();
		} catch (ConfigurationManagerException e) {
			e.getMessage();
		}
		ResultadoSimulacion[] listaResultadosSemana = new ResultadoSimulacion[dias.length];
		String[] diasDeLaSemana = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"};
		
		//Genera la prueba segun los dias de la semana configurados en el txt
		for (int i = 0; i < dias.length; i++) {
			listaResultadosSemana[i] = this.ejecutar(dias[i]);
		
			System.out.println("Resultados de simulación del día " + (diasDeLaSemana[i]) + ":");
			System.out.println("Tiempo Atencion Promedio: " + listaResultadosSemana[i].getPromedioTiempoAtencion());
			System.out.println("Tiempo Espera Completo: " + listaResultadosSemana[i].getPromedioEsperaServicioCompleto());
			System.out.println("Tiempo Espera Economico: " + listaResultadosSemana[i].getPromedioEsperaServicioEconomico());
			System.out.println("Tiempo Espera Encerado: " + listaResultadosSemana[i].getPromedioEsperaServicioEncerado());
			System.out.println("Tiempo Espera Premium: " + listaResultadosSemana[i].getPromedioEsperaServicioPremium());
			System.out.println("Tiempo de espera General: " + listaResultadosSemana[i].getPromedioEsperaGeneralEspera());
			System.out.println("Costo Diario: " + listaResultadosSemana[i].getCostoDiario());
			System.out.println("Promedio Costo Servicios: " + listaResultadosSemana[i].promedioTotalCostosServicio());
			
			System.out.println("Cantidad de encerados: " + ServicioEncerado.getCantidadDeVecesUsado());
			System.out.println("---------------------------------------------");
		}
	}

	
	private double promedioTiempos(int tiempoTotalEspera, int cantidadVecesUsado){
		double resultado = 0;
		
		if (cantidadVecesUsado != 0) {
			resultado = tiempoTotalEspera/cantidadVecesUsado;
		}
		return resultado;
	}



	
	
}