package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import servicios.ServicioCompleto;
import servicios.ServicioEconomico;
import servicios.ServicioEncerado;
import servicios.ServicioPremium;
import estaciones.Brillo;
import estaciones.Encerado;
import estaciones.LavadoEnjuage;
import estaciones.LavadoLlantas;
import estaciones.Prelavado;
import estaciones.Secado;



public class ConfigurationManager{
	private String ruta;
	
	public ConfigurationManager(String ruta){
		this.ruta = ruta;
	}
	
	public void inicializarAtributosDeServicios(){
		ServicioEconomico.setTiempoEsperaTotal(0);
		ServicioPremium.setTiempoEsperaTotal(0);
		ServicioEncerado.setTiempoEsperaTotal(0);
		ServicioCompleto.setTiempoEsperaTotal(0);
		
		ServicioEconomico.setCantidadDeVecesUsado(0);
		ServicioPremium.setCantidadDeVecesUsado(0);
		ServicioEncerado.setCantidadDeVecesUsado(0);
		ServicioCompleto.setCantidadDeVecesUsado(0);
		
	}
	
	public void configurarEstaciones() throws ConfigurationManagerException{
		FileReader archivo = null;
		BufferedReader lineas;
		String linea;
		
		try {
			archivo = new FileReader(this.ruta);
			lineas = new BufferedReader(archivo);
			
			linea = lineas.readLine();
			while (linea != null && !linea.trim().isEmpty()) {
				StringTokenizer str = new StringTokenizer(linea, ":");	
				
				this.completarTablaLavados(str);
				
				linea = lineas.readLine();
			}		
			
			archivo.close();
		} catch (FileNotFoundException e) {
			throw new ConfigurationManagerException("Archivo no encontrado: No existe el archivo/directorio especificado");
		} catch (IOException e) {
			throw new ConfigurationManagerException("Error al leer la linea: Se produjo un error al tratar de leer una linea del archivo");
		}
		
	}
	
	public int[] configurarDias() throws ConfigurationManagerException{
		FileReader archivo = null;
		BufferedReader lineas;
		String linea;
		
		int[] dias = new int[6];
		
		try {
			archivo = new FileReader(this.ruta);
			lineas = new BufferedReader(archivo);
			
			for(int i = 0; i < 7 ; i++){
				lineas.readLine();
			}
			
			linea = lineas.readLine();
			
			for (int i = 0; i < dias.length && linea != null; i++) {
				StringTokenizer str = new StringTokenizer(linea, ":");
				str.nextToken();
								
				dias[i] = Integer.parseInt(str.nextToken());
				
				linea = lineas.readLine();
				
			}
			
			archivo.close();
		} catch (FileNotFoundException e) {
			throw new ConfigurationManagerException("Archivo no encontrado: No existe el archivo/directorio especificado");
		} catch (IOException e) {
			throw new ConfigurationManagerException("Error al leer la linea: Se produjo un error al tratar de leer una linea del archivo");
		}
		
		return dias;
		
	}
	
	
	private void completarTablaLavados(StringTokenizer linea){
		String token = linea.nextToken();
		StringTokenizer valores = new StringTokenizer(linea.nextToken(), ",");
		
		switch (token) {
			case "P":
				Prelavado.cargarEstacion(Integer.parseInt(valores.nextToken()),Integer.parseInt(valores.nextToken()) );		
				break;
			case "LyE":
				LavadoEnjuage.cargarEstacion(Integer.parseInt(valores.nextToken()), Integer.parseInt(valores.nextToken()));
				break;
			case "LL":
				LavadoLlantas.cargarEstacion(Integer.parseInt(valores.nextToken()), Integer.parseInt(valores.nextToken()));
				break;
			case "S":
				Secado.cargarEstacion(Integer.parseInt(valores.nextToken()), Integer.parseInt(valores.nextToken()));	
				break;
			case "B":
				Brillo.cargarEstacion(Integer.parseInt(valores.nextToken()), Integer.parseInt(valores.nextToken()));			
				break;
			case "E":
				Encerado.cargarEstacion(Integer.parseInt(valores.nextToken()), Integer.parseInt(valores.nextToken()));			
				break;
				
			default:
				break;
		}

	}

	
}
