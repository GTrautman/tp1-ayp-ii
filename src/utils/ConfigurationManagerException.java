package utils;

public class ConfigurationManagerException  extends Exception{
	
	private static final long serialVersionUID = 1L;
	public ConfigurationManagerException(String msg){
		super(msg);
	}

}
