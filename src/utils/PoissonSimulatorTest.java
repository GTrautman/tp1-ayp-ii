package utils;

import junit.framework.TestCase;

public class PoissonSimulatorTest extends TestCase {

	private PoissonSimulator p;
	int cant;
	int min;
	
	public PoissonSimulatorTest(String n){
		super(n);
	}
	
protected void setUp() throws PoissonSimulatorException {
		
		cant = 100; //Arriban 100 clientes
		min = 60*10; //En 600 minutos (10 hs)
		p = new PoissonSimulator(cant, min);
			
		}

protected void tearDown(){
	p = null;
}

	public void testPromedio(){
		int cantSimulaciones = 100*cant; //Cantidad de simulaciones
		int i;
		double acumulado=0, promedio, actual;
		System.out.println("Test Frecuencia promedio de arribo");
		for (i = 1; i <= cantSimulaciones; i++){
			actual=p.proximoArribo();
			acumulado += actual;
		}
		promedio = acumulado / cantSimulaciones;
		System.out.println("Frecuencia = " +((double)cant)/min);
		System.out.println("Frecuencia promedio = "+1.0/promedio);
		
	}
	
	public void testCantidad(){
		int cantSimulaciones= 100;
		double actual;
		int i, cantidad,promedio, acumulado=0;
		System.out.println("Test cantidad promedio de arribos en "+min+" minutos");
		for (i=0; i<cantSimulaciones; i++){
			cantidad=0;
			for (actual=0.0; actual <=min; actual+=p.proximoArribo()){
				cantidad++;
			}
			acumulado+=cantidad;
			System.out.println("Simulacion "+i+": ");
			System.out.println("\t#arribos = "+cantidad);
		}
		promedio=acumulado/cantSimulaciones;
		System.out.println("Promedio de arribos en "+ cant+" minutos = " +promedio);
	}

}
