package utils;
import java.util.ArrayList;

import clases.Ticket;


public class Cola {
	private ArrayList<Ticket> datos;
	
	/**
	 * Cola generica FIFO
	 * Si hay espacio en memoria siempre permite
	 * agregar elementos
	 */
	public Cola(){
		datos = new ArrayList<Ticket>();
	}
	
	/**
	 * Cola con tama�o especifico
	 * Si tamanio es negativo levanta una excepcion
	 * Si se desea controlar el tamaño de la Cola, lo debe
	 * hacer el que usa la clase Cola
	 */
	public Cola(int tamanio) throws ColaException{
		try{
			datos=new ArrayList<Ticket>(tamanio);
		} catch (IllegalArgumentException e) {
			throw new ColaException("Tamaño Invalido: No se puede crear la cola");
		}
	}
	
	/**
	 * Devuelve verdadero si la cola esta vacia
	 */
	public boolean colaVacia(){
		return datos.isEmpty();
	}
	
	/**
	 * Encola un objeto
	 * Si el objeto a encolar es nulo, levanta una excepcion
	 */
	public void encolar(Ticket o) throws ColaException{
		if (o == null){
			throw new ColaException("Dato inválido: No se puede encolar un dato nulo");
		}
		datos.add(o);
	}
	
	/**
	 * Desencola un objeto
	 * Si la cola esta vacia, levanta una excepcion
	 */
	public Ticket desencolar() throws ColaException{
		
		if (this.colaVacia()){
			throw new ColaException("Desencolar: La cola esta vacia");
		}
		return(datos.remove(0));
		
	}
	
	//Agregamos un metodo a la cola para obtener el primer objeto sin desencolar
	public Ticket getPrimero() throws ColaException{
		if(this.colaVacia()){
			throw new ColaException("Obtener primero: La cola esta vacia");
		}
		return datos.get(0);
	}
	
	public int largo(){
		return datos.size();
	}

}
