package tests;

import utils.ConfigurationManager;
import utils.ConfigurationManagerException;

import org.junit.Test;

public class ConfigurationManagerTest {
	String file = "D:\\Facu\\lavado.txt";
	
	@Test
	public void probarLecturaDeArchivo(){
		ConfigurationManager file = new ConfigurationManager(this.file);
		try {
			file.configurarEstaciones();
		} catch (ConfigurationManagerException e) {
			e.getMessage();
		}
	}
	
	@Test
	public void probarLecturaDeArchivoNulo(){
		ConfigurationManager file = new ConfigurationManager(null);
		try {
			file.configurarEstaciones();
		} catch (ConfigurationManagerException e) {
			e.getMessage();
		}
	}
	
	@Test
	public void probarLecturaDeArchivoDias(){
		ConfigurationManager file = new ConfigurationManager(this.file);
		try {
			file.configurarDias();
		} catch (ConfigurationManagerException e) {
			e.getMessage();
		}
	}
}
