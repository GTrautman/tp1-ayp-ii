package tests;
import org.junit.Test;

import servicios.ServicioEconomico;
import servicios.ServicioPremium;
import clases.Lavadero;
import clases.Ticket;
//import clases.ResultadoSimulacion;

public class ColaMaquinaTst {

	@Test
	public void llegadaPersonaTest(){
		int tiempoServicio = 4;
		Ticket persona = new Ticket(tiempoServicio, 0);
		persona.setServicio(new ServicioEconomico());
		
		System.out.println(persona.getRequiereServicioOpcional());
		System.out.println("");
		//LEVANTAR ARCHIVO DE CONFIGURACIÓN E INICIAR SIMULACIÓN
		
		System.out.println("Economico:"+ServicioEconomico.getCantidadDeVecesUsado());
		System.out.println("Premium:"+ServicioPremium.getCantidadDeVecesUsado());
		System.out.println("");
	}
	
	@Test
	public void ejecutarSimulacionTst(){
		Lavadero maquina = new Lavadero();
		maquina.ejecutar(5);
	}
	
	/*@Test
	public void ejecutarMaquinaEnteraTest(){
		System.out.println("");
		
		Lavadero maquina = new Lavadero();
		maquina.iniciarSimulacion();
	}*/
}
