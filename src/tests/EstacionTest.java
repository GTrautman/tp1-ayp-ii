package tests;
import org.junit.Test;

import clases.Ticket;
import estaciones.Brillo;
import estaciones.Encerado;
import estaciones.LavadoEnjuage;
import estaciones.LavadoLlantas;
import estaciones.Prelavado;
import estaciones.Secado;

public class EstacionTest {

	@SuppressWarnings("static-access")
	@Test
	public void probarEstaciones(){
		Prelavado.cargarEstacion(30, 25);
		Brillo.cargarEstacion(10, 15);
		LavadoEnjuage.cargarEstacion(20, 10);
		LavadoLlantas.cargarEstacion(100, 80);
		Secado.cargarEstacion(15, 12);
		Encerado.cargarEstacion(5, 3);
		Ticket pers1 = new Ticket(12, 10);
		
		System.out.println("valor PreLavado: " +Prelavado.precio);
		System.out.println("valor Brillo: " +Brillo.precio);
		System.out.println("valor LavadoEnjuague: " +LavadoEnjuage.precio);
		System.out.println("valorLLantas: " +LavadoLlantas.precio);
		System.out.println("El Servicio es: " + pers1.getServicio() + "el tiempo es: " +
		pers1.getTiempoAtencion());
		System.out.println("El Servicio es: " + pers1.getServicio() + "el costo es: " +
		pers1.getServicio().getPrecioTotal());
	}
	
}