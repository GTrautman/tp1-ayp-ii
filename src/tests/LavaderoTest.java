package tests;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import clases.Lavadero;
import clases.ResultadoSimulacion;

public class LavaderoTest {

	//No es un test, es una prueba del programa
	@Test
	public void lavaderoTest() {
		Lavadero lavadero = new Lavadero();
		lavadero.iniciarSimulacion();
	}
	
	@Test
	public void lavaderoTestValorProximoAuto(){
		Lavadero lavadero = new Lavadero();
		ResultadoSimulacion resultado = new ResultadoSimulacion();
		assertEquals(lavadero.ejecutar(700).getClass(), resultado.getClass());
	}
		
}
