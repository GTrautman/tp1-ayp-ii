package tests;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import utils.ColaException;
import clases.Maquina;
import clases.Ticket;

public class MaquinaTest {

	@Test
	public void maquinaVaciaTest(){
		Maquina maquina = new Maquina();
		assertEquals(true, maquina.maquinaVacia());
	}
	
	@Test
	public void evaluarLlegadaTest() throws ColaException{
		Maquina maquina = new Maquina();
		maquina.evaluarLlegada(7);
		assertEquals(false, maquina.maquinaVacia());
		assertEquals(7, maquina.tiempoOcioso);
		Ticket ticketTest = new Ticket(12, 1);
		assertEquals(ticketTest.getClass(), maquina.obtenerTop().getClass());
		
	}
	
}
