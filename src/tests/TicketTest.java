package tests;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import servicios.Servicio;
import clases.Ticket;

public class TicketTest {

	@Test
	//Test Random, puede dar OK o no
	public void ticketTestGetServicioOpcional(){
		Ticket ticket = new Ticket(7,0);
		assertEquals(false, ticket.getRequiereServicioOpcional());
	}
	
	@Test
	public void ticketTestGetServicio(){
		Ticket ticket = new Ticket(10,0);
		assertEquals(Servicio.class, ticket.getServicio().getClass().getSuperclass());
	}	

	@Test
	public void ticketTestHoraLlegada(){
		Ticket ticket = new Ticket(5,10);
		assertEquals(10, ticket.getHoraLlegada());
	}
}
