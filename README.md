# Trabajo Práctico N°1 de Algoritmos y Programación II #

Este repositorio incluye el código fuente del TP para Algoritmos y Programación II de la UNTREF.

### Contenido ###

* Clases
* Wiki

### Como configurar Eclipse para utilizarlo ###

Para configurar Eclipse seguir esta [http://crunchify.com/how-to-configure-bitbucket-git-repository-in-you-eclipse/](guía de crunchify) para configurarlo, siguiendo los pasos desde el 4 hasta el 9. En el lugar donde dice URI tomar el link de https que esta en el perfil de cada uno arriba a la derecha en bitbucket

### Puntos a realizar ###

* UMLs
* Programación
* Estadísticas
* Revisión y optimización
* Tests
* Informe

